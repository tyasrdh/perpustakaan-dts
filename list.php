<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Buku</title>
    <link href="simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body style="font-family: Georgia,Times,Times New Roman,serif;">
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Admin Panel</div>
      <div class="list-group list-group-flush">
        <a href="list.php" class="list-group-item list-group-item-action bg-light">Data Buku</a>
        <a href="kategori.php" class="list-group-item list-group-item-action bg-light">Data Kategori Buku</a>
        <a href="logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <div class="container"><br/>
        <h1>Daftar Buku</h1>
        <a href="create.php" class="btn btn-primary mb-2"> Tambah Buku</a>
        <a href="home.php" class="btn btn-danger mb-2"> Kembali</a>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">No</th>
                <th scope="col">ID Kategori</th>
                <th scope="col">Judul Buku</th>
                <th scope="col">Cover Buku</th>
                <th scope="col">Detail Buku</th>
                <th scope="col">Pengarang</th>
                <th scope="col">Penerbit</th>
                <th scope="col">Tahun Rilis</th>
                <th scope="col">Stok</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <?php
                include "akses.php";
                include "koneksi.php";
                // START get data from table produk
                $sql = "SELECT * from buku ORDER BY buku_id";
                // END get data from table produk

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                foreach ($hasil as $key => $data) {
                    ?>
                    <tbody>
                        <tr>
                            <td><?php echo $key + 1 ?></td>
                            <td><?php echo $data['kategori_id'] ?></td>
                            <td><?php echo $data['buku_judul'] ?></td>
                            <td><?php echo $data['buku_image']?></td>
                            <td><?php echo $data['buku_detail']?></td>
                            <td><?php echo $data['pengarang']?></td>
                            <td><?php echo $data['penerbit']?></td>
                            <td><?php echo $data['tahun']?></td>
                            <td><?php echo $data['stok']?></td>
                            <td>
                                <a href="edit.php?buku_id=<?php echo $data['buku_id']?>" class="btn btn-warning">Ubah</a>
                                <a href="<?php echo $_SERVER['PHP_SELF']?>?buku_id=<?php echo $data['buku_id']?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                    </tbody>
                <?php
                }
                if (isset($_GET['buku_id'])) {
                    # code...
                    $id = $_GET['buku_id'];
                    $sql = "DELETE FROM buku WHERE buku_id=$id";
                    $hasil = mysqli_query($db, $sql);
                    if ($hasil) {
                        echo("<script>location.href = 'list.php';</script>");
                    } else {
                        echo "<div class='alert alert-danger'> Data gagal dihapus. </div>";
                    }
                }
                ?>
        </table>
        
    </div>
</div>
</body>
</html>