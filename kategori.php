<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Kategori Buku</title>
    <link href="simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body style="font-family: Georgia,Times,Times New Roman,serif;">
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Admin Panel</div>
      <div class="list-group list-group-flush">
        <a href="list.php" class="list-group-item list-group-item-action bg-light">Data Buku</a>
        <a href="kategori.php" class="list-group-item list-group-item-action bg-light">Data Kategori Buku</a>
        <a href="logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <div class="container"><br/>
        <h1>Daftar Kategori Buku</h1>
        <a href="create_kategori.php" class="btn btn-primary mb-2"> Tambah Kategori</a>
        <a href="home.php" class="btn btn-danger mb-2"> Kembali</a>
        
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">Kategori ID</th>
                <th scope="col">Nama Kategori</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <?php
                include "akses.php";
                include "koneksi.php";

                // START get data from table produk
                $sql = "SELECT * from kategori ORDER BY kategori_id";
                // END get data from table produk

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                foreach ($hasil as $key => $data) {
                    ?>
                    <!-- START isi data -->
                    <tbody>
                        <tr>
                        <td><?php echo $key + 1 ?></td>
                        <td><?php echo $data['kategori_nama'] ?></td>
                        <td>
                            <a href="edit_kategori.php?kategori_id=<?php echo $data['kategori_id']?>" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                        </tr>
                    </tbody>
                    <!-- END isi data -->
                <?php
                }
                // END mengeksekusi data
                ?>
        </table>
        
    </div>
</div>
</body>
</html>