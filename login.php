<?php 
session_start(); 
include "koneksi.php";

if (isset($_POST['uname']) && isset($_POST['password'])) {

	function validate($data){
       $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}

	$uname = validate($_POST['uname']);
	$pass = validate($_POST['password']);

	if (empty($uname)) {
		header("Location: index.php?error=username harus diisi");
	    exit();
	}else if(empty($pass)){
        header("Location: index.php?error=password harus diisi");
	    exit();
	}else{
		$sql = "SELECT * FROM admin WHERE username='$uname' AND password='$pass'";

		$result = mysqli_query($db, $sql);

		if (mysqli_num_rows($result) === 1) {
			$row = mysqli_fetch_assoc($result);
            if ($row['username'] === $uname && $row['password'] === $pass) {
            	$_SESSION['username'] = $row['username'];
            	$_SESSION['name'] = $row['name'];
            	$_SESSION['id'] = $row['id'];
            	header("Location: list.php");
		        exit();
            }else{
				header("Location: index.php?error=username atau password yang dimasukan salah");
		        exit();
			}
		}else{
			header("Location: index.php?error=username atau password yang dimasukan salah");
	        exit();
		}
	}
	
}else{
	header("Location: index.php");
	exit();
}