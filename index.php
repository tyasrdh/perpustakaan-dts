<?php
$connect = mysqli_connect("localhost", "root", "", "perpustakaan");
$tab_query = "SELECT * FROM kategori ORDER BY kategori_id ASC";
$tab_result = mysqli_query($connect, $tab_query);
$tab_menu = '';
$tab_content = '';
$i = 0;
while($row = mysqli_fetch_array($tab_result)) {
     if($i == 0){
        $tab_menu .= '<li class="active"><a href="#'.$row["kategori_id"].'" data-toggle="tab">'.$row["kategori_nama"].'</a></li>';
        $tab_content .= '<div id="'.$row["kategori_id"].'" class="tab-pane fade in active">';
    }
    else {
        $tab_menu .= '<li><a href="#'.$row["kategori_id"].'" data-toggle="tab">'.$row["kategori_nama"].'</a></li>';
        $tab_content .= '<div id="'.$row["kategori_id"].'" class="tab-pane fade">
        ';
    }

    $buku_query = "SELECT * FROM buku WHERE kategori_id = '".$row["kategori_id"]."'";
    $buku_result = mysqli_query($connect, $buku_query);
    while($sub_row = mysqli_fetch_array($buku_result)) {
        $tab_content .= '
        <div class="col-md-3" style="margin-bottom:36px;">
        <img src="images/'.$sub_row["buku_image"].'" class="img-responsive img-thumbnail" width="200" height="232" data-toggle="modal"   data-target="#myModal'.$row["kategori_id"].'" style="cursor: pointer"/>
        <h6>'.$sub_row["buku_judul"].'</h6>
        <h6>Stok : '.$sub_row["stok"].'</h6>
            <div id="myModal'.$row["kategori_id"].'" class="modal fade modaldal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">'.$sub_row["buku_judul"].'</h3>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                        <img src="images/'.$sub_row["buku_image"].'" class="img-responsive img-thumbnail"/>
                        </div>
                        <br/><h4>'.$sub_row["buku_detail"].'</h4>
                        <br/><p>Jumlah stok tersedia : '.$sub_row["stok"].'</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </div>

                </div>
            </div>
        </div>';
    }
 $tab_content .= '<div style="clear:both"></div></div>';
 $i++;
}
?>

<!DOCTYPE html>
<html>
 <head>
  <title>PERPUSTAKAAN</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
 </head>
 <body style="font-family: Georgia,Times,Times New Roman,serif;">
 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Perpustakaan ABC</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
    </ul>
  </div>
</nav>
<div class="container">
   <h2 align="center">BUKU PERPUSTAKAAN ABC</a></h2>
   <br />
   <ul class="nav nav-tabs">
        <?php echo $tab_menu; ?>
   </ul>
   <div class="tab-content">
        <br />
        <?php echo $tab_content; ?>
   </div>
</div>
</body>
</html>
