<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Buku</title>
    <link href="simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body style="font-family: Georgia,Times,Times New Roman,serif;">
<div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">Admin Panel</div>
            <div class="list-group list-group-flush">
            <a href="list.php" class="list-group-item list-group-item-action bg-light">Data Buku</a>
            <a href="kategori.php" class="list-group-item list-group-item-action bg-light">Data Kategori Buku</a>
            <a href="logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->
    <div class="container">
        <?php
            include "koneksi.php";
            include "akses.php";

            // START cek apakah ada kiriman form dari method POST
            // function input($data) {
            //     $data = trim($data);
            //     $data = stripcslashes($data);
            //     $data = htmlspecialchars($data);
            //     return $data;
            // }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $id     = $_POST["buku_id"];
                $judul  = $_POST["buku_judul"];
                $kategori  = $_POST["kategori_id"];
                $image  = $_POST["buku_image"];
                $detail   = $_POST["buku_detail"];
                $pengarang = $_POST["pengarang"];
                $penerbit  = $_POST["penerbit"];
                $tahun = $_POST["tahun"];
                $stok  = $_POST["stok"];

                $sql = "UPDATE buku SET  buku_judul='$judul',kategori_id='$kategori',buku_image='$image',buku_detail='$detail',pengarang='$pengarang',penerbit='$penerbit',tahun='$tahun',stok='$stok' WHERE buku_id=$id"; 

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                // END mengeksekusi data

                // START cek hasil eksekusi
                if ($hasil) {
                    header("Location:list.php");
                } else {
                    echo "<div class='alert alert-danger'> Data gagal diubah. </div>";
                }
                // END cek hasil eksekusi
            }
            // END cek apakah ada kiriman form dari method POST

            // START get data edit
            if (isset($_GET['buku_id'])) {
                $id = $_GET['buku_id'];

                $sql = "SELECT * FROM buku where buku_id = $id";
                $hasil = mysqli_query($db, $sql);
                $data = mysqli_fetch_assoc($hasil);
            }
            // END get data edit

        ?>
<br/>
<h4>Ubah Buku</h4>
        <form action="edit.php" method="post" id="form">
        <a href="list.php" class="btn btn-warning"> Kembali</a>
        <br><br>
        <input type="hidden" name="buku_id" value="<?php echo $data['buku_id'] ?>">  
            <div class="form-group">
                <label for="judul">Judul Buku</label>
                <input type="text" name="buku_judul" placeholder="Masukkan Judul Buku" class="form-control" id="" required value="<?php echo $data['buku_judul'] ?>">
            </div>
            <div class="form-group">
                <label for="image">ID Kategori</label>
                <input type="text" name="kategori_id" placeholder="Masukkan ID Kategori" class="form-control" id="" required value="<?php echo $data['kategori_id'] ?>">
            </div>
            <div class="form-group">
                <label for="image">Gambar Buku</label>
                <input type="file" name="buku_image" placeholder="Masukkan Gambar" class="form-control" id="" required value="<?php echo $data['buku_image'] ?>">
            </div>
            <div class="form-group">
                <label for="detail">Detail Buku</label>
                <input type="text" class="form-control" name="buku_detail" placeholder="Masukkan Detail buku" class="form-control" id="" required value="<?php echo $data['buku_detail'] ?>">
            </div>
            <div class="form-group">
                <label for="pengarang">Pengarang</label>
                <input type="text" name="pengarang" placeholder="Masukkan Pengarang Buku" class="form-control" id="" required value="<?php echo $data['pengarang'] ?>">
            </div>
            <div class="form-group">
                <label for="penerbit">Penerbit</label>
                <input type="text" name="penerbit" placeholder="Masukkan Penerbit Buku" class="form-control" id="" required value="<?php echo $data['penerbit'] ?>">
            </div>
            <div class="form-group">
                <label for="tahun">Tahun Rilis</label>
                <input type="text" name="tahun" placeholder="Masukkan Tahun Buku Dibuat" class="form-control" id="" required value="<?php echo $data['tahun'] ?>">
            </div>
            <div class="form-group">
                <label for="stok">Stok</label>
                <input type="text" name="stok" placeholder="Masukkan Jumlah Stok Tersedia" class="form-control" id="" required value="<?php echo $data['stok'] ?>">
            </div>
            
            <button type="reset" placeholder="Masukkan merek" class="btn btn-danger">Reset</button>
            <button type="submit" placeholder="Masukkan merek" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</div>
</body>
</html>