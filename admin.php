<!DOCTYPE html>
<html>
<head>
	<title>LOGIN</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="font-family: Georgia,Times,Times New Roman,serif;">
     <form action="login.php" method="post">
     	<h2>LOGIN PETUGAS</h2>
     	<?php if (isset($_GET['error'])) { ?>
     		<p class="error"><?php echo $_GET['error']; ?></p>
     	<?php } ?>
     	<label>Username</label>
     	<input type="text" name="uname" placeholder="username" required><br>

     	<label>Password</label>
     	<input type="password" name="password" placeholder="password" required><br>

     	<button type="submit" class="btn btn-primary">Login</button>
     </form>
</body>
</html>