<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Kategori</title>
    <link href="simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('#btn-ulangi').click(function(){
                $("input[name=kategori_id]").val('');
                $("input[name=kategori_nama]").val('');
            });
        });
    </script>
<body style="font-family: Georgia,Times,Times New Roman,serif;">
<div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">Admin Panel</div>
            <div class="list-group list-group-flush">
            <a href="list.php" class="list-group-item list-group-item-action bg-light">Data Buku</a>
            <a href="kategori.php" class="list-group-item list-group-item-action bg-light">Data Kategori Buku</a>
            <a href="logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->
    <div class="container">
        <?php
            include "akses.php";
            include "koneksi.php";

            // START cek apakah ada kiriman form dari method POST
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $kategori_nama  = $_POST["kategori_nama"];
                $sql = "INSERT into kategori (kategori_nama) VALUES 
                       ('$kategori_nama')"; 

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                // END mengeksekusi data

                // START cek hasil eksekusi
                if ($hasil) {
                    header("Location:kategori.php");
                } else {
                    echo "<div class='alert alert-danger'> Data gagal disimpan. </div>";
                }
                // END cek hasil eksekusi
            }
            // END cek apakah ada kiriman form dari method POST

        ?>
    <br/>
        <h5>Tambah Kategori</h5>
        <a href="kategori.php" class="btn btn-warning"> Kembali</a>
        <br><br>
        <form action="create_kategori.php" method="post" id="form">
            <div class="form-group">
                <label for="judul">Nama Kategori</label>
                <input type="text" name="kategori_nama" placeholder="Masukkan Nama Kategori" class="form-control" id="" required>
            </div>
            
            
            <button type="reset" placeholder="Masukkan merek" class="btn btn-danger">Reset</button>
            <button type="submit" placeholder="Masukkan merek" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    </div>
</body>
</html>